// Décrementation des besoins en timmer avec possibilité de redonner des points de vie


let foodDom = document.querySelector('#jaugedefaim');
let sleepDom = document.querySelector('#jaugedesommeil');
let soinDom = document.querySelector('#jaugedhygiène');
let liDom = document.querySelector('#li');

// Boutons pour redonner des points et bouton start
const nourritureButton = document.getElementById('nourriture');
const canapeButton = document.getElementById('canape');
const doucheButton = document.getElementById('douche');
const start = document.getElementById('start');
const resetButtum = document.getElementById('reset');

var food = 10;
var sleep = 20;
var soin = 15;

let intervalFood;
let intervalSleep;
let intervalSoin;


// bouton start
function play() {

// Timmer d'alimentation
intervalFood = window.setInterval(function() { 
     if (food > 0)
         { 
            food=food-1;
            foodDom.innerText = 'Mon niveau de faim ' + food;
 } else {
     clearInterval(intervalFood);
 } }, 1000  );

 nourritureButton.addEventListener('click', function() {
    console.log(nourritureButton);
    food = food + 3;
});

// Timmer  de sommeil
intervalSleep = window.setInterval(function() {
     if (sleep > 0)
         {
             sleep=sleep-1;
             sleepDom.innerText = 'Mon niveau de sommeil ' + sleep;
 }else {
     clearInterval(intervalSleep);
 } }, 1000  );

canapeButton.addEventListener('click', function() {
    console.log(canapeButton);
    sleep = sleep + 4;
});
 
//Timmer Hygiène
intervalSoin = window.setInterval(function() { 
    console.log(soin);
     if (soin > 0)
         {
             soin=soin-1;
             soinDom.innerText = 'Mon niveau d\'hygiène ' + soin;
 }else {
     window.clearInterval(intervalSoin);
 } }, 1000  );

doucheButton.addEventListener('click', function() {
    console.log(doucheButton);
    soin = soin + 2;
});
}

start.addEventListener('click', function() {
    play();
})

// bouton reset 

function resetPlay() {
    // annule l'action puis lui redéfinir une valeur
    clearInterval(intervalFood);
    food = 10;
    clearInterval(intervalSleep);
    sleep = 15;
    clearInterval(intervalSoin);
    soin = 20;

    foodDom.innerText ='Manger';
    sleepDom.innerText ='canapé';
    soinDom.innerText = 'hygiène';
    
}

resetButtum.addEventListener('click', function() {
    resetPlay();
})



 // bouton HTML   
    /* <div class="button" id="button">

<P class="entretien">Occupe toi de moi </P>
<div class="button3" id ="button3">
<button id="nourriture">Nourriture</button>
<button id="canape">Canapé</button>
<button id="douche">Douche</button>


<div class="startplay">
    <button id="start" class="start">Play</button>
    <button id="reset" class="reset">Reset</button>
</div>

</div>

</div> */


// Jauge

var food = 100;
var sleep = 100;
var soin = 100;

// Boutons pour redonner des points, bouton start et bouton reset
const nourritureButton = document.getElementById('nourriture');
const canapeButton = document.getElementById('canape');
const doucheButton = document.getElementById('douche');
const start = document.getElementById('start');
const resetButtum = document.getElementById('reset');


let barFaimDom = document.getElementById('barFaim');
let barDodoDom = document.getElementById('barDodo');
let barDoucheDom = document.getElementById('barDouche');

let intervalBarFood;
let intervalBarSleep;
let intervalBarDouche;


// Jauge Dynamique avec bouton play
function play() {
    // jauge de faim 
    intervalBarFood = window.setInterval(function () {
        if (food > 0) {
            food = food - 3;
            barFaimDom.style.width = food + '%';
        } else {
            clearInterval(intervalBarFood);
        }
    }, 1000);
    // bouton pour lui donner à manger 
    nourritureButton.addEventListener('click', function () {
        if (food < 100) {
            food = food + 3;
        } else {
            food = food + 0;
        }
    });

    // jauge de sommeil
    intervalBarSleep = window.setInterval(function () {
        if (sleep > 0) {
            sleep = sleep - 1;
            barDodoDom.style.width = sleep + '%';
        } else {
            clearInterval(intervalBarSleep);
        }
    }, 1000);
    // bouton pour le faire dormir
    canapeButton.addEventListener('click', function () {
        if (sleep < 100) {
            sleep = sleep + 2;
        } else {
            sleep = sleep + 0;
        }
    });


    // jauge de propeté
    intervalBarDouche = window.setInterval(function () {
        if (soin > 0) {
            soin = soin - 3;
            barDoucheDom.style.width = soin + '%';
        } else {
            clearInterval(intervalBarFood);
        }
    }, 2000);
    // bouton pour le laver
    doucheButton.addEventListener('click', function () {
        if (soin < 100) {
            soin = soin + 3;
        } else {
            soin = soin + 0;
        }
    });

}

//bouton play
start.addEventListener('click', function () {
    play();
})


// bouton reset 

function resetPlay() {
    // annule l'action puis lui redéfinir une valeur
    clearInterval(intervalBarFood);
    food = 100;
    clearInterval(intervalBarSleep);
    sleep = 100;
    clearInterval(intervalBarDouche);
    soin = 100;
}

resetButtum.addEventListener('click', function () {
    resetPlay();
})